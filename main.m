xi = 46;
%% Prodiagrafes
CL = 2 + 0.01 * xi; %pF
SRmin = 18 + 0.01 * xi; %V/us
Vdd = 1.8 + 0.003 * xi; %V
Vss = -(1.8 + 0.003 * xi); %V
GBmin = 7 + 0.01 * xi; %MHz
Amin = 20 + 0.01 * xi; %dB
Pmax = 50 + 0.01 * xi; %mW

Vin_max = 0.1;
Vin_min = -0.1;


%% PMOS 
Kp = 2.9352e-5;
LAMBDA_p = 0.04;
Vtp = -0.9056;
Cox = Kp / (180.2 * 1e-4);
%% NMOS
Kn = 9.6379e-5;
LAMBDA_n = 0.05;
Vtn = 0.786;

%% main
L = 1e-6; %m
CCmin = 0.22 * CL; %pF

%% W3 & W4
I5 = SRmin * CCmin * 1e-6; %A
S3 = I5/(Kp *(Vdd - Vin_max - abs(Vtp) + Vtn)^2);
if (S3<1)
    S3 = 1;
end
S4=S3;

W3 = S3 * L;
W4 = W3;

%% p3
I3 = I5/2;
p3 = (sqrt(2*Kp*S3*I3)/(2*0.0667*(L^(2))*S3*Cox))/(2*pi); %Hz
if (p3 > 10 * GBmin * 1e6)
    disp('p3 PASSED');
end

%% W1 & W2
gm1 = 2*pi*GBmin*CCmin*1e-6;
gm2=gm1;
S1 = gm1^2/(Kn*I5);
if (S1<1)
S1 = 1;
end
S2=S1;
W1 = S1 * L;
W2 = W1;

%% W5 & M8
Vds5_sat = Vin_min - Vss - sqrt(I5/(Kn*S1)) - Vtn;

S5=(2*I5)/(Kn*Vds5_sat^2);
if (S5<1)
S5=1;
end
W5=S5*L;
W8 = W5;

%% W6
gm6 = 2.2*gm1*(CL/CCmin); %gm2 = gm1
gm4 = sqrt(2*Kp*S3*I3); %S4 = S3
S6 = S3*(gm6/gm4); %S4 = S3
W6=S6*L;

%% W7
I6 = (gm6^2)/(2*Kp*S6);
S7 = (I6*S5)/I5;
W7=S7*L;

%% Gain
Av=20*log10((2*gm2*gm6)/(I5*((LAMBDA_n+LAMBDA_p)^2)*I6));
if (Av > Amin)
    disp('GAIN PASSED');
end

%% Power
Pdissipated=(I5+I6)*(Vdd+abs(Vss));
if (Pdissipated < Pmax * 1e-3)
    disp('POWER PASSED');
end
